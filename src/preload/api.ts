import { writeFileSync, mkdir, rm, existsSync } from 'node:fs';
import { Buffer } from 'node:buffer';
import { exec } from 'node:child_process';

const SAVEDIR = '/Users/lia/Movies/Glance_Down';

/**
 * url: m3u8 link
 * callback: (status, info)
 * status:
 *    - 204 video已存在
 *    - 200 ts下载成功
 *    - 305 ts下载完后, 写入错误
 *    - 404 ts资源未找到
 *    - 601 ts开始往mp4, 合并转化
 *    - 602 视频转化完成
 *    - 609 视频转化失败
*/
async function download(url: string, callback: Function) {
  if(!url.includes('.m3u8')) return;
  // 获取m3u8所在的线上目录
  let url_list = url.split('/');
  let m3u8_name = url_list.pop();
  let m3u8_path = url_list.join('/');

  let file_txt:string[] = [];
  let file_name = m3u8_name?.split('.')[0];
  let file_temp = `${SAVEDIR}/${file_name}`;

  let info = {
    sub: 0,
    total: 0,
    percentage: '0%',
    downloading: '',
    detail: {
      save_dir: SAVEDIR,
      save_name: file_name,
      m3u8: url,
    }
  }

  let rs = await fetch(url);
  let m3u8_txt = await rs.text();
  console.log(m3u8_txt)

//   let m3u8_txt = `
// #EXTM3U
// #EXT-X-VERSION:3
// #EXT-X-TARGETDURATION:14
// #EXT-X-MEDIA-SEQUENCE:0
// #EXTINF:13.920000,
// 5bc57cf1166c5a455a7ee2baab523dd10.ts
// #EXTINF:6.200000,
// sfdauwhiuw.ts
// 5bc57cf1166c5a455a7ee2baab523dd11.ts
// #EXTINF:9.880000,
// 5bc57cf1166c5a455a7ee2baab523dd12.ts
// #EXT-X-ENDLIST
// //   `
  // 整理m3u8文本中的ts文件，转为数组备用
  let m3u8_list = m3u8_txt.split(/[(\r\n\s)\r\n\s]+/);
  let ts_list = m3u8_list.filter(item => item.includes('.ts'));
  let ts_list_fullpath = ts_list.map((item: string): string => {
    let r = item;
    if(item.includes('http')) {
      r = item;
    }else if(item.split('/').length == 1) {
      r = m3u8_path + '/' + item;
    }

    return r;
  })
  let ts_list_fullpath_length = ts_list_fullpath.length;
  info.total = ts_list_fullpath_length;


  // 声明递归函数，用于下载 .ts 文件
  async function ts_loader (index: number = 0) {
    if(index >= ts_list_fullpath_length) return;

    let item = ts_list_fullpath[index];
    if(!item) return;
    let tsList = item.split('/');
    let tsName = tsList[tsList.length-1];

    // 开始请求.ts数据
    console.log(`[下载${index}-开始] ${tsName}`);
    // 处理下载进度
    info.sub = index;
    info.percentage = ((info.sub / info.total) * 100).toFixed(2) + '%';
    info.downloading = `${file_temp}/${tsName}`;
    callback(200, info);

    let res = await fetch(item);
    if(res.status == 200) {
      // 请求成功后，转为buffer，写入本地文件
      let buf = await res.arrayBuffer();
      let data = Buffer.from(buf);
  
      try {
        writeFileSync(`${file_temp}/${tsName}`, data);
        file_txt.push(`file ${tsName}`);
        console.log(`[下载${index}-结束] ${tsName}`);

        // 处理下载进度
        info.sub = index + 1;
        info.percentage = ((info.sub / info.total) * 100).toFixed(2) + '%';
        callback(200, info);
      } catch (e) {
        console.error(`[写入${index}-错误]: `, e);
        callback(305, e);
      }
    }else {
      // 请求错误，丢出错误
      console.log(`[下载${index}-错误] ${tsName}`);
      callback(res.status, res)
    }

    if(index + 1 < ts_list_fullpath_length) {
      // 如后续还有数据，递归处理
      ts_loader(index + 1);
    }else {
      info.downloading = '';
      // 全部下载完成后，使用ffmpeg合并视频为mp4
      file_txt.unshift("ffconcat version 1.0");
      try{
        writeFileSync(`${file_temp}/file.txt`, file_txt.join("\n"))

        console.log("[转化-开始] 下载完成, 正在转化为mp4文件...");
        callback(601, info);
        exec(`cd ${file_temp} &&  ffmpeg -i file.txt -acodec copy -vcodec copy -absf aac_adtstoasc ${SAVEDIR}/${file_name}.mp4`,function(error, stdout){
          if(error){
              console.error("[转化-失败] ",error);
              callback(609, info);
          }else{
              console.log("[转化-成功] ",stdout);
              callback(602, info);
              //删除临时文件
              rm(file_temp, { force: true, recursive: true }, (err) => {
                if (err) throw err;
                console.log('[临时文件-删除]')
              })
          }
        });
      }catch(e){
        console.error("写入file.txt错误: ",e);
        return ;
      }
    }
  }

  // 默认请求第一个数据
  if(ts_list_fullpath_length >= 1) {
    // 判断有无已下载的视频
    let exit = existsSync(`${SAVEDIR}/${file_name}.mp4`);
    if(exit) {
      console.log(`[已存在] ${file_name}.mp4 `);
      callback(204, `[已存在] ${file_name}.mp4`)
      return;
    };

    // 创建视频下载的临时文件
    mkdir(file_temp, { recursive: true }, (err) => {
      if (err) throw err;
      ts_loader()
    })
  }
}

export default {
  download
}